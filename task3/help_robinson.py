import requests
import os
from string import punctuation as sc

words_to_find = ['island','islands']

try:
	data = open('data.txt').read()	#read data if available locally
except FileNotFoundError:
	# If not found locally fetch it from the url and store it
	os.system('wget -O data.txt http://www.gutenberg.org/files/521/521-0.txt')
	data = open('data.txt').read()

clean_data = data.lower()			#converting all words in it's lowercase form

for c in sc:
	clean_data = clean_data.replace(c," ")	#taking care of special characters 

clean_data = clean_data.replace("—"," ")	#removing "—" character. 

list_of_words = clean_data.split()	#getting all words out of the clean data

with open('islands.txt','w') as file:
	for word in words_to_find:
		data_to_written = word + " " + str(list_of_words.count(word)) + "\n"
		file.write(data_to_written)
	file.close()