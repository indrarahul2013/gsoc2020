#!/bin/bash

if [ "$EUID" -ne 0 ]; then
	echo "Please run as a root!"
	exit
else
	echo "Running as a Root.....!"
fi

if [[ $(grep -E --color 'vmx|svm' /proc/cpuinfo) ]]; then
	echo "Virtualization is supported!"
else
	echo "Virtualization is not supported on this machine."
	echo "Exiting..."
	exit
fi

if ! [ -x "$(command -v kubectl)" ]; then
	echo "kubectl not Found. Installing ...."
	curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
	chmod +x ./kubectl
	sudo mv ./kubectl /usr/local/bin/kubectl
	echo "kubectl installed !!"
else
	echo "kubectl Found."
fi

if ! [ -x "$(command -v minikube)" ]; then
  echo "minikube not Found. Installing ...."
  curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
  chmod +x minikube
  sudo install minikube /usr/local/bin/
  sudo rm minikube
else
	echo "minikube Found."
fi

sudo mv /root/.kube /home/$USER/.kube
sudo mv /root/.minikube /home/$USER/.minikube

sudo chown -R $USER $HOME/.kube $HOME/.minikube

echo "Starting minikube!"
minikube start --vm-driver kvm2

echo "minikube Started !!"

echo "Exiting...."