In Task #1 we already had webserver running up. We somehow wanted to push the metrics from this webserver to running instance of Grafana.

For that we have three more containers running :-
- node_vts_exporter
- prom/prometheus
- grafana/grafana

We enabled vts modules on nginx container which will push nginx webserver metrics in json format.
on localhost/status/format/json.
Now running node_vts_exporter pulls nginx/status/format/json and exposes on exporter/metrics in a format which prometheus understands.
Prometheus pulls the exporter/metrics from nginx_vts_exporter and makes it available for Grafana.
Grafana queries prometheus and using graphs it shows metrics.

All this setup can be visualized using this flowchart

![](flow.png)